//
//  ViewController.swift
//  20190417-AnthonyOlukitibi-NYCSchools
//
//  Created by ANTHONY O. on 4/17/19.
//  Copyright © 2019 ANTHONY O. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var schools = [NYCSchool]()
    var schoolResults = [SchoolResult]()
    
    @IBOutlet weak var schoolTableView: UITableView! {
        didSet {
            schoolTableView.dataSource = self
            schoolTableView.delegate = self
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "NYC Schools"
        fetchSchoolInfoDataFromServer()
        fetchSchoolResultsDataFromServer()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SchoolTableViewCell", for: indexPath)
        
        if schools.isEmpty == false {
            let school = schools[indexPath.row]
            if let schoolName = school.schoolName {
                cell.textLabel?.text = schoolName
            }
            if let schoolLocation = school.location, let endIndex = schoolLocation.range(of: "(")?.lowerBound {
                cell.detailTextLabel?.text = String(schoolLocation[..<endIndex])
            }
            cell.textLabel?.numberOfLines = 0
            cell.textLabel?.lineBreakMode = .byWordWrapping
            cell.detailTextLabel?.numberOfLines = 0
            cell.detailTextLabel?.lineBreakMode = .byWordWrapping
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let school = schools[indexPath.row]
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyBoard.instantiateViewController(withIdentifier: "SchoolDetailsViewController") as! SchoolDetailsViewController
        viewController.school = school
        viewController.schoolResults = schoolResults
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func fetchSchoolInfoDataFromServer() {
        let urlString = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json"
        let decoableObject = "NYCSchool"
        getDataFromURL(forDecodableObject: decoableObject, withURLString: urlString) { [weak self] (objects, error) in
            if objects != nil {
                self?.schools = objects as! [NYCSchool]
                DispatchQueue.main.async {
                    self?.schoolTableView.reloadData()
                    print("Table and collection has Reloaded")
                }} else if (error != nil) {
                print("This is Error \(String(describing: error))")
                self?.displayAlertMessage(titleToDisplay: "An Error Occured", messageToDisplay: "Please try again later, thank you!")
            }
        }
    }
    
    func fetchSchoolResultsDataFromServer() {
        let urlString = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        let decoableObject = "SchoolResult"
        getDataFromURL(forDecodableObject: decoableObject, withURLString: urlString) { [weak self] (objects, error) in
            if objects != nil {
                self?.schoolResults = objects as! [SchoolResult]
            } else if (error != nil) {
                print("This is Error \(String(describing: error))")
                self?.displayAlertMessage(titleToDisplay: "An Error Occured", messageToDisplay: "Please try again later, thank you!")
            }
        }
    }
    
    func displayAlertMessage(titleToDisplay: String, messageToDisplay: String) {
        let alertController = UIAlertController(title: titleToDisplay, message: messageToDisplay, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "OK", style: .default) { (_) in
            self.presentingViewController?.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion:nil)
    }
    
}

