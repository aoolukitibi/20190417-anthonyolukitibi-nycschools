//
//  ResponseModel.swift
//  20190417-AnthonyOlukitibi-NYCSchools
//
//  Created by ANTHONY O. on 4/17/19.
//  Copyright © 2019 ANTHONY O. All rights reserved.
//

import Foundation

struct NYCSchool: Decodable {
    let dbn: String?
    let schoolName: String?
    let overviewParagraph: String?
    let location: String?
    let phoneNumber: String?
    let schoolEmail: String?
}

struct SchoolResult: Decodable {
    let dbn: String?
    let schoolName: String?
    let numOfSatTestTakers: String?
    let satCriticalReadingAvgScore: String?
    let satMathAvgScore: String?
    let satWritingAvgScore: String?
}

func getDataFromURL(forDecodableObject: String, withURLString: String, completionhandler:@escaping (Any?, Error?) -> ()) {
    let url = URL(string: withURLString)
    let request = NSMutableURLRequest(url:url!)
    let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
        if let httpResponse = response as? HTTPURLResponse {
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                var dataObjects = Array<Any>()
                if forDecodableObject == "NYCSchool" {
                    dataObjects = try decoder.decode([NYCSchool].self, from: data!)
                } else if forDecodableObject == "SchoolResult" {
                    dataObjects = try decoder.decode([SchoolResult].self, from: data!)
                }
                completionhandler(dataObjects, nil)
            } catch let error as NSError {
                print(error.localizedDescription)
                completionhandler(nil, error)
            }
            print("The status code is \(httpResponse.statusCode)")
        } else if (error != nil) {
            print(error!.localizedDescription)
            completionhandler(nil, error)
        }
    }
    task.resume()
}
