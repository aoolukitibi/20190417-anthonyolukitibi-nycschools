//
//  SchoolDetailsViewController.swift
//  20190417-AnthonyOlukitibi-NYCSchools
//
//  Created by ANTHONY O. on 4/17/19.
//  Copyright © 2019 ANTHONY O. All rights reserved.
//

import UIKit

class SchoolDetailsViewController: UIViewController {
    
    var school: NYCSchool?
    var schoolResults = [SchoolResult]()
    
    @IBOutlet weak var schoolName: UILabel!
    @IBOutlet weak var schoolAddress: UILabel!
    @IBOutlet weak var schoolPhoneNumber: UILabel!
    @IBOutlet weak var schoolEmail: UILabel!
    @IBOutlet weak var schoolOverview: UILabel!
    @IBOutlet weak var numOfTestTakers: UILabel!
    @IBOutlet weak var criticalReadingAvgScore: UILabel!
    @IBOutlet weak var mathAvgScore: UILabel!
    @IBOutlet weak var writingAvgScore: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpDetailViewWithSchoolInfo()
    }
    
    func setUpDetailViewWithSchoolInfo() {
        if let school = school {
            if let name = school.schoolName {
                schoolName.text = name
            }
            if let address = school.location, let endIndex = address.range(of: "(")?.lowerBound {
                schoolAddress.text = String(address[..<endIndex])
            }
            if let phoneNumber = school.phoneNumber {
                schoolPhoneNumber.text = phoneNumber
            }
            if let email = school.schoolEmail {
                schoolEmail.text = email
            }
            if let overview = school.overviewParagraph {
                schoolOverview.text = "Overview: \(overview)"
            }
        }
        
        var schoolResult: SchoolResult?
        let schoolDBN = school?.dbn
        for schoolRst in schoolResults {
            if schoolDBN == schoolRst.dbn {
                schoolResult = schoolRst
                break
            }
        }
        
        let testTakersNum = schoolResult?.numOfSatTestTakers
        numOfTestTakers.text = "SAT Scores \n\nNumber of SAT Test Takers: \(testTakersNum ?? "Not Avialable" )"
        
        let readingScore = schoolResult?.satCriticalReadingAvgScore
        criticalReadingAvgScore.text = "Critical Reading Average Score: \(readingScore ?? "Not Available")"
        
        let mathScore = schoolResult?.satMathAvgScore
        mathAvgScore.text = "Math Average Score: \(mathScore ?? "Not Available")"
        
        let writingScore = schoolResult?.satWritingAvgScore
        writingAvgScore.text = "Writing Average Score: \(writingScore ?? "Not Available")"
    }
    
}
